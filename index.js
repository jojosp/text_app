
const max_length = 40

new Vue({  
    el: '#app',
      data: {
        text: ''
        
      },
      computed: {
        textIsEmpty() {
          return this.text.length === 0;
        },
        charactersRemaining() {
          return max_length - this.text.length
        },
        exceededTest(){
          if(this.charactersRemaining>=0&&this.text.length !=0){
            return false
          }else return true
        },
        underTwentyMark: function() {
          return this.charactersRemaining <= 20 
            && this.charactersRemaining > 10;
          },
        underTenMark: function() {
          return this.charactersRemaining <= 10 && this.charactersRemaining >= 0;
        }

      },
      methods: {
        clear() {
          this.text=''
        }
      }
  })